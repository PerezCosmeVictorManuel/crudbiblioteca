<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>CREATE libro</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $error = false;
  $isbn = $_POST['isbn'];
  $titulo_libro = $_POST['titulo_libro'];

  if (empty($isbn)) {
    $error = true;
?>
  <p>Error, no se indico el ISBN del libro</p>
<?php
  }
  if (empty($titulo_libro)) {
    $error = true;
?>
  <p>Error, no se indico el titulo del libro</p>
<?php
  }

  if (!$error) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select isbn
      from biblioteca.libro
      where isbn = '".$isbn."';";

    $libro = pg_query($query) or die('La consulta falló: ' . pg_last_error());

    if (pg_num_rows($libro) == 1) {
?>
  <p>Error, ya se encuentra registrado un libro con ISBN <?php echo $isbn; ?></p>
<?php
    } else {
      $query = "insert into biblioteca.libro values('".$isbn."', '".$titulo_libro."');";

      $resultado = pg_query($query) or die('La consulta falló: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al momento de guardar los datos del libro</p>
<?php
      } else {
?>
  <p>El libro con ISBN <?php echo $isbn; ?> y titulo "<?php echo $titulo_libro; ?>" ha sido guardado con exito.</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="formulario-libro.php">Nuevo libro</a></li>
</ul>

</body>
</html>