<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>CREATE ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>

<?php
  $error = false;
  $clave_ejemplar = $_POST['clave_ejemplar'];
  $conservacion_ejemplar = $_POST['conservacion_ejemplar'];
  $isbn = $_POST['isbn'];

  if (empty($clave_ejemplar)) {
    $error = true;
?>
  <p>Error, no se indico la clave del ejemplar</p>
<?php
  }
  if (empty($conservacion_ejemplar)) {
    $error = true;
?>
  <p>Error, no se indico la conservacion del ejemplar</p>
<?php

  }
  if (empty($isbn)) {
    $error = true;
?>
  <p>Error, no se indico el isbn del libro</p>
<?php

  }


  if (!$error) {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar
      from biblioteca.ejemplar
      where clave_ejemplar = '".$clave_ejemplar."';";

    $ejemplar= pg_query($query) or die('La consulta fall�: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 1) {
?>
  <p>Error, ya se encuentra registrado un ejemplar con esta clave <?php echo $clave_ejemplar; ?></p>
<?php
    } else {
      $query = "insert into biblioteca.ejemplar values('".$clave_ejemplar."', '".$conservacion_ejemplar."', '".$isbn ."');";

      $resultado = pg_query($query) or die('La consulta fall�: ' . pg_last_error());

      if (pg_affected_rows($resultado) == 0) {
?>
  <p>Error al guardar los datos del ejemplar</p>
<?php
      } else {
?>
  <p>El ejemplar con clave <?php echo $clave_ejemplar; ?> con conservacion "<?php echo $conservacion_ejemplar; ?> y isbn "<?php echo $isbn;?>" ha sido guardado con exito.</p>
<?php
      }
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="formulario-ejemplar.php">Nuevo ejemplar</a></li>
</ul>

</body>
</html>