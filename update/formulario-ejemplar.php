<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
  <title>Formulario Ejemplar</title>
  <link rel="stylesheet" type="text/css" href="../css/estilo.css" />
</head>
<body>
<?php
  $clave_ejemplar = $_GET['clave_ejemplar'];
  $isbn=$_GET['isbn'];


  if (empty($isbn)) {
?>
  <p>Error, no se ha indicado isbn</p>
<?php
  } 
  if (empty($clave_ejemplar)) {
?>
  <p>Error, no se ha indicado clave</p>
<?php
  } 
 
  else {
    $servidorbd = "localhost";
    $nombrebd = "prueba";
    $usuariobd= "programador";
    $contraseniabd = "12345";

    $dbconn = pg_connect("host=$servidorbd dbname=$nombrebd user=$usuariobd password=$contraseniabd")
    or die('No se ha podido conectar: ' . pg_last_error());

    $query = "select clave_ejemplar, conservacion_ejemplar,isbn
      from biblioteca.ejemplar
      where isbn = '".$isbn."' and clave_ejemplar='".$clave_ejemplar."';";

    $ejemplar= pg_query($query) or die('La consulta fall�: ' . pg_last_error());

    if (pg_num_rows($ejemplar) == 0) {
?>
  <p>No se ha encontrado alg�n ejemplar<?php echo $clave_ejemplar; ?></p>
<?php
    } else {
      $tupla = pg_fetch_array($ejemplar, null, PGSQL_ASSOC);
      $clave_ejemplar        =$tupla['clave_ejemplar'];
      $conservacion_ejemplar = $tupla['conservacion_ejemplar'];
      $isbn                  =$tupla['isbn'];

?>
<form action="update-ejemplar.php" method="post">
<table>
  <caption>Informaci�n de ejemplar</caption>
  <tbody>
    <tr>
      <th>clave</th>
      <td><input type="text" name="clave_ejemplar" value="<?php echo $clave_ejemplar; ?>" /></td>
    </tr>
    <tr>
      <th>conservacion</th>
      <td><textarea name="conservacion_ejemplar"><?php echo $conservacion_ejemplar; ?></textarea></td>
    </tr>
    <tr>
      <th>isbn</th>
      <td><input type="text" name="isbn" value="<?php echo $isbn; ?>" /></td>
    </tr>
  </tbody>
</table>
<input type="submit" name="submit" value="UPDATE" />
</form>
<?php
    }
  }
?>

<ul>
  <li><a href="../inicio.html">Regresar al inicio</a></li>
  <li><a href="ejemplares.php">Lista de ejemplares</a></li>
</ul>

</body>
</html>